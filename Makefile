SHELL := /bin/bash -O globstar


run:
	hypercorn --reload --config=hypercorn.toml 'resplendent_quetzal.main:app'


test:
	pytest -x --cov-report term-missing --cov-report html --cov-branch \
	       --cov resplendent_quetzal/


lint:
	@echo
	ruff .
	@echo
	blue --check --diff --color .
	@echo
	mypy .
	@echo
	pip-audit


format:
	ruff --silent --exit-zero --fix .
	blue .


build:
	docker build -t resplendent_quetzal .


smoke_test: build
	docker run --rm -d -p 5000:5000 --name resplendent_quetzal resplendent_quetzal
	sleep 4; curl http://localhost:5000/hello
	docker stop resplendent_quetzal


install_hooks:
	@ scripts/install_hooks.sh
