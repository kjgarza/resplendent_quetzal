Feature: API Completion Endpoints

  Scenario: Request completion from the completions endpoint
    Given the API is up and running
    And the user is authorized
    When the user sends a valid request to the /api/v1/completions endpoint with a specific prompt
    Then the API should return a valid completion response based on the provided prompt

  Scenario: Request with overridden defaults at the completions endpoint
    Given the API is up and running
    And the user is authorized
    When the user sends a request to the /api/v1/completions endpoint with overridden default values
    Then the API should return a completion response reflecting the overridden defaults

  Scenario: Request completion from the metadata translation endpoint with default values
    Given the API is up and running
    And the user is authorized
    When the user sends a request to the /api/v1/metadata/translation endpoint without specifying defaults
    Then the API should use the pre-set default system message, assistant message, and model for the response

  Scenario: Request completion from the metadata enrichment endpoint with default values
    Given the API is up and running
    And the user is authorized
    When the user sends a request to the /api/v1/metadata/enrichment endpoint without specifying defaults
    Then the API should use the pre-set default system message, assistant message, and model for the response

  Scenario: Authorized vs non-authorized user model defaults
    Given the API is up and running
    When a request is sent by an authorized user to any endpoint
    Then the default model for the authorized user should be used
    When a request is sent by a non-authorized user to any endpoint
    Then a different default model should be used for the non-authorized user

  Scenario: Sending a request with a specific payload structure to the completions endpoint
    Given the API is up and running
    When the user sends a request to the /api/v1/completions endpoint with a payload following the specified structure
    Then the API should correctly interpret and respond based on the 'messages', 'model', and other parameters in the payload
