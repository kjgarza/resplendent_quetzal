import unittest
from unittest.mock import patch

from fastapi.testclient import TestClient

from resplendent_quetzal.routers import static


class TestStatic(unittest.TestCase):
    @patch('resplendent_quetzal.services.static_files_generator.generate_ai_plugin_json')
    def test_get_ai_plugin_json(self, mock_generate_ai_plugin_json):
        mock_generate_ai_plugin_json.return_value = {'key': 'value'}
        client = TestClient(static.router)
        response = client.get('/.well-known/ai-plugin.json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'key': 'value'})

    @patch('resplendent_quetzal.services.static_files_generator.generate_openapi_yaml')
    def test_get_openapi_yaml(self, mock_generate_openapi_yaml):
        mock_generate_openapi_yaml.return_value = 'openapi: 3.0.0'
        client = TestClient(static.router)
        response = client.get('/openapi.yaml')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.text, 'openapi: 3.0.0')

    def test_get_logo_png(self):
        client = TestClient(static.router)
        response = client.get('/logo.png')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['content-type'], 'image/png')

    def test_read_root(self):
        client = TestClient(static.router)
        response = client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['content-type'], 'text/html; charset=utf-8')

    def test_read_goldie(self):
        client = TestClient(static.router)
        response = client.get('/goldie')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['content-type'], 'text/html; charset=utf-8')

    def test_get_inmediasres_svg(self):
        client = TestClient(static.router)
        response = client.get('/inmediasres.svg')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['content-type'], 'image/svg+xml')

if __name__ == '__main__':
    unittest.main()
