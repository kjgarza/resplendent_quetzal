import unittest
from unittest.mock import AsyncMock, patch

from fastapi.testclient import TestClient

from resplendent_quetzal.routers import graphql


class TestGraphQL(unittest.TestCase):
    @patch('resplendent_quetzal.services.strategy_integration.StrategyIntegration', new_callable=AsyncMock)
    async def test_repositories_success(self, mock_strategy_integration):
        mock_strategy_integration.fetch_data.return_value = (200, {'data': {'repositories': 'success'}})
        client = TestClient(graphql.router)
        response = client.post('/graphql/', json={'query': '{ repositories }'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'data': {'repositories': 'success'}})

    @patch('resplendent_quetzal.services.strategy_integration.StrategyIntegration', new_callable=AsyncMock)
    async def test_repository_success(self, mock_strategy_integration):
        mock_strategy_integration.fetch_data.return_value = (200, {'data': {'repository': 'success'}})
        client = TestClient(graphql.router)
        response = client.post('/graphql/', json={'query': '{ repository }'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'data': {'repository': 'success'}})

if __name__ == '__main__':
    unittest.main()
