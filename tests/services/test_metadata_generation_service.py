import base64
from unittest.mock import AsyncMock, MagicMock

import pytest
from pydantic import BaseModel

from resplendent_quetzal.services.metadata_generation_service import (
    MetadataGenerationService,
)


class User(BaseModel):
    is_authorized: bool


@pytest.fixture
def mock_model_selector():
    mock = MagicMock()
    mock.select_model.return_value = ('model', 'model_client')
    return mock


@pytest.fixture
def mock_model_client():
    mock = AsyncMock()
    mock.fetch_completion = AsyncMock(return_value='mocked_response')
    return mock


@pytest.fixture
def mock_prompts_service():
    mock = MagicMock()
    mock.get_prompts.return_value = {'generate': {'system': 'default_system_message'}}
    return mock


@pytest.fixture
def metadata_generation_service(mock_model_selector, mock_model_client, mock_prompts_service):
    service = MetadataGenerationService(User(is_authorized=True))
    service.model_selector = mock_model_selector
    service.model_client = mock_model_client
    service.get_default_system_message = MagicMock(return_value='default_system_message')
    return service


@pytest.mark.asyncio
async def test_generate(metadata_generation_service):
    request = MagicMock()
    request.context = base64.b64encode(b'test_context').decode('utf-8')
    request.target_schema = 'target_schema'

    response = await metadata_generation_service.generate(request)

    assert response == 'mocked_response'
    metadata_generation_service.model_client.fetch_completion.assert_called_once()


def test_get_default_system_message(metadata_generation_service, mock_prompts_service):
    result = metadata_generation_service.get_default_system_message()
    assert result == 'default_system_message'
    # mock_prompts_service.get_prompts.assert_called_once_with('kjgarza/prompts', 'parrot_gpt.yml')
