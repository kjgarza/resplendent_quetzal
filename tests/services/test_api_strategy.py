import pytest
import requests_mock

from resplendent_quetzal.services.api_strategy import (
    CrossrefRESTStrategy,
    DataciteGraphQLStrategy,
)


# Testing CrossrefRESTStrategy
def test_crossref_rest_strategy_success():
    strategy = CrossrefRESTStrategy()
    with requests_mock.Mocker() as m:
        m.get('https://api.crossref.org/works/test_query', json={'status': 'ok', 'message': 'success'}, status_code=200)
        status_code, message = strategy.fetch('test_query')
        assert status_code == 200
        assert message == 'success'


def test_crossref_rest_strategy_failure():
    strategy = CrossrefRESTStrategy()
    with requests_mock.Mocker() as m:
        m.get('https://api.crossref.org/works/test_query', status_code=404)
        with pytest.raises(ValueError) as excinfo:
            strategy.fetch('test_query')
        assert 'Failed to fetch data from Crossref' in str(excinfo.value)


# Testing DataciteGraphQLStrategy
def test_datacite_graphql_strategy_success():
    strategy = DataciteGraphQLStrategy()
    with requests_mock.Mocker() as m:
        m.post('https://api.datacite.org/graphql', json={'data': 'success'}, status_code=200)
        status_code, data = strategy.fetch('test_query')
        assert status_code == 200
        assert data == {'data': 'success'}


def test_datacite_graphql_strategy_failure():
    strategy = DataciteGraphQLStrategy()
    with requests_mock.Mocker() as m:
        m.post('https://api.datacite.org/graphql', status_code=404)
        with pytest.raises(ValueError) as excinfo:
            strategy.fetch('test_query')
        assert 'Failed to fetch data from Datacite' in str(excinfo.value)


# Testing specific method in DataciteGraphQLStrategy
def test_datacite_return_grei_repositories():
    strategy = DataciteGraphQLStrategy()
    with requests_mock.Mocker() as m:
        expected_response = {
            'data': {
                'repositories': {
                    'totalCount': 1,
                    'nodes': [
                        {'uid': '123', 'name': 'Mendeley', 'description': 'A repository', 'url': 'http://mendeley.com'}
                    ]
                }
            }
        }
        m.post('https://api.datacite.org/graphql', json=expected_response, status_code=200)
        status_code, data = strategy.return_grei_repositories()
        assert status_code == 200
        assert data == expected_response
