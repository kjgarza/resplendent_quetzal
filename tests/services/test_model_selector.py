from typing import List

from pydantic import BaseModel

from resplendent_quetzal.services.completion_service import CompletionService
from resplendent_quetzal.services.metadata_enrichment_service import (
    MetadataEnrichmentService,
)
from resplendent_quetzal.services.metadata_generation_service import (
    MetadataGenerationService,
)
from resplendent_quetzal.services.metadata_translation_service import (
    MetadataTranslationService,
)
from resplendent_quetzal.services.model_selector import (
    CompletionsModelStrategy,
    MetadataEnrichmentModelStrategy,
    MetadataGenerationModelStrategy,
    MetadataTranslationModelStrategy,
    ModelSelector,
    NonAuthorizedUserModelStrategy,
)


class Message(BaseModel):
    role: str
    content: str


class User(BaseModel):
    is_authorized: bool


class Request(BaseModel):
    model: str
    messages: List[Message]


class TestModelSelector:
    def test_select_model_metadata_translation_service_authorized(self):
        model_selector = ModelSelector()
        user = User(is_authorized=True)
        user.is_authorized = True
        service_context = MetadataTranslationService(user)
        model = model_selector.select_model(service_context, user)
        assert model[1].__class__ == MetadataTranslationModelStrategy().select_model()[1].__class__

    def test_select_model_metadata_translation_service_unauthorized(self):
        model_selector = ModelSelector()
        user = User(is_authorized=False)
        user.is_authorized = False
        service_context = MetadataTranslationService(user)
        model = model_selector.select_model(service_context, user)
        assert model[1].__class__ == NonAuthorizedUserModelStrategy().select_model()[1].__class__

    def test_select_model_metadata_enrichment_service_authorized(self):
        model_selector = ModelSelector()
        user = User(is_authorized=True)
        user.is_authorized = True
        service_context = MetadataEnrichmentService(user)
        model = model_selector.select_model(service_context, user)
        assert model[1].__class__ == MetadataEnrichmentModelStrategy().select_model()[1].__class__

    def test_select_model_metadata_enrichment_service_unauthorized(self):
        model_selector = ModelSelector()
        user = User(is_authorized=False)
        user.is_authorized = False
        service_context = MetadataEnrichmentService(user)
        model = model_selector.select_model(service_context, user)
        assert model[1].__class__ == NonAuthorizedUserModelStrategy().select_model()[1].__class__

    def test_select_model_metadata_generation_service_authorized(self):
        model_selector = ModelSelector()
        user = User(is_authorized=True)
        user.is_authorized = True
        service_context = MetadataGenerationService(user)
        model = model_selector.select_model(service_context, user)
        assert model[1].__class__ == MetadataGenerationModelStrategy().select_model()[1].__class__

    def test_select_model_completions_service_authorized(self):
        model_selector = ModelSelector()
        user = User(is_authorized=True)
        user.is_authorized = True
        service_context = CompletionService(user)
        model = model_selector.select_model(service_context, user)
        assert model[1].__class__ == CompletionsModelStrategy().select_model()[1].__class__

    def test_select_model_completions_service_unauthorized(self):
        model_selector = ModelSelector()
        user = User(is_authorized=False)
        user.is_authorized = False
        service_context = CompletionService(user)
        model = model_selector.select_model(service_context, user)
        assert model[1].__class__ == NonAuthorizedUserModelStrategy().select_model()[1].__class__
