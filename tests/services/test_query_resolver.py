from unittest.mock import patch

import pytest

from resplendent_quetzal.services.query_resolver import QueryResolver


@pytest.fixture
def query_resolver():
    return QueryResolver()


@pytest.fixture
def mock_crossref_rest_strategy():
    with patch('resplendent_quetzal.services.api_strategy.CrossrefRESTStrategy') as mock:
        yield mock


@pytest.fixture
def mock_datacite_graphql_strategy():
    with patch('resplendent_quetzal.services.api_strategy.DataciteGraphQLStrategy') as mock:
        yield mock


def test_resolve_journal_article(query_resolver, mock_crossref_rest_strategy):
    mock_crossref_rest_strategy.return_value.fetch.return_value = (200, 'crossref_result')
    strategy, status_code, result = query_resolver.resolve('journalArticle', '?rows=0')

    assert strategy.__class__.__name__ == 'CrossrefRESTStrategy'
    assert status_code == 200
    assert 'facets' in result


def test_resolve_datasets(query_resolver, mock_datacite_graphql_strategy):
    mock_datacite_graphql_strategy.return_value.fetch.return_value = (200, 'datacite_result')
    strategy, status_code, result = query_resolver.resolve('datasets', '{ repositories { totalCount } }')

    assert strategy.__class__.__name__ == 'DataciteGraphQLStrategy'
    assert status_code == 200
    assert result == {'data': {'repositories': {'totalCount': 5035}}}


def test_resolve_unsupported_query_type(query_resolver):
    with pytest.raises(ValueError) as excinfo:
        query_resolver.resolve('unsupported_type', 'test_query')
    assert 'Unsupported query type: unsupported_type' in str(excinfo.value)


@patch('resplendent_quetzal.services.cache_resolver')
def test_caching_behavior(mock_cache_resolver, query_resolver):
    mock_cache_resolver.return_value = lambda x: x  # Mock the decorator to return the function as is
    query_resolver.resolve('journalArticle', '?rows=0')

    # Check if the cache_resolver decorator was called
    # mock_cache_resolver.assert_called()
