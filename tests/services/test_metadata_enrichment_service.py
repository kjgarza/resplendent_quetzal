from unittest.mock import MagicMock

import pytest
from pydantic import BaseModel

from resplendent_quetzal.services.metadata_enrichment_service import (
    MetadataEnrichmentService,
)


class User(BaseModel):
    is_authorized: bool


@pytest.fixture
def mock_model_selector():
    mock = MagicMock()
    mock.select_model.return_value = ('model', 'model_client')
    return mock


@pytest.fixture
def mock_model_client():
    mock = MagicMock()
    mock.fetch_completion.return_value = 'mocked_response'
    return mock


@pytest.fixture
def mock_prompts_service():
    mock = MagicMock()
    mock.get_prompts.return_value = {'enrich': {'system': 'default_system_message'}}
    return mock


@pytest.fixture
def metadata_enrichment_service(mock_model_selector, mock_model_client, mock_prompts_service):
    service = MetadataEnrichmentService(User(is_authorized=True))
    service.model_selector = mock_model_selector
    service.model_client = mock_model_client
    service.get_default_system_message = MagicMock(return_value='default_system_message')
    return service


def test_enrich(metadata_enrichment_service):
    request = MagicMock()
    request.messages = [{'role': 'user', 'content': 'test_message'}]

    response = metadata_enrichment_service.enrich(request)

    assert response == 'mocked_response'
    metadata_enrichment_service.model_client.fetch_completion.assert_called_once()


def test_get_default_system_message(metadata_enrichment_service, mock_prompts_service):
    result = metadata_enrichment_service.get_default_system_message()
    assert result == 'default_system_message'
    # mock_prompts_service.get_prompts.assert_called_once_with('kjgarza/prompts', 'parrot_gpt.yml')
