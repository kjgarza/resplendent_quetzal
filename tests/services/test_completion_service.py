from unittest.mock import MagicMock

from resplendent_quetzal.services.completion_service import CompletionService


class MockModelSelector:
    def select_model(self, service, user, request):
        return 'model', MagicMock()

class MockModelClient:
    async def fetch_completion(self, model, messages):
        return 'Response'

def test_generate_completion():
    service = CompletionService('user')
    service.model_selector = MockModelSelector()
    service.model_client = MockModelClient()

    request = MagicMock()
    request.messages = [{'role': 'user', 'content': 'User message'}]

    response = service.generate_completion(request)

    assert response == 'Response'
    service.model_selector.select_model.assert_called_once_with(service, 'user', request)
    service.model_client.fetch_completion.assert_called_once_with('model', [{'role': 'system', 'content': 'text2text-generation'}, {'role': 'user', 'content': 'User message'}])
