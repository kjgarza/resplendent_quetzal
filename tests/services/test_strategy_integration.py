from unittest.mock import patch

import pytest

from resplendent_quetzal.services.strategy_integration import StrategyIntegration


@pytest.fixture
def mock_query_resolver():
    with patch('resplendent_quetzal.services.query_resolver.QueryResolver') as mock:
        yield mock.return_value


@pytest.fixture
def strategy_integration():
    return StrategyIntegration()


@pytest.mark.asyncio
async def test_fetch_data_success(strategy_integration, mock_query_resolver):
    mock_query_resolver.resolve.return_value = ('strategy', 200, 'result_data')
    status_code, result = await strategy_integration.fetch_data('journalArticle', '?rows=0')

    assert status_code == 200
    assert 'facets' in result
    # mock_query_resolver.resolve.assert_called_once_with('journalArticle', '?rows=0')


@pytest.mark.asyncio
async def test_fetch_data_failure(strategy_integration, mock_query_resolver):
    mock_query_resolver.resolve.return_value = ('strategy', 404, None)
    with pytest.raises(ValueError) as excinfo:
        await strategy_integration.fetch_data('journalArticle', 'fail_query')
    assert 'Failed to fetch data from Crossref' in str(excinfo.value)
    # mock_query_resolver.resolve.assert_called_once_with('journalArticle', 'fail_query')
