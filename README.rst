=====================================
GraphQL Gateway API for Scholarly Records
=====================================

Welcome to the GraphQL Gateway API project! This API is designed to serve as a gateway to aggregate scholarly records from various underlying data sources.

Features
--------

- Receive GraphQL queries and decide which underlying API to query based on the type of scholarly record requested.
- Interface with both REST and GraphQL backend services.
- Efficient and streamlined error handling.

Setup & Installation
--------------------

1. Clone the repository:
   .. code-block:: bash

      git clone https://gitlab.com/kjgarza/resplendent_quetzal.git

2. Navigate to the project directory:
   .. code-block:: bash

      cd resplendent_quetzal

3. Install the required packages:
   .. code-block:: bash

      pip install -r requirements.txt

4. Run the server:
   .. code-block:: bash

      hypercorn main:app --reload

Visit `http://127.0.0.1:5001/graphql` to access the GraphQL playground and make queries.

Citation
--------

If you use this software in your research or project, please cite it. See the `CITATION.cff` file for more details.

License
-------

This project is licensed under the MIT License.

Author
------

Kristian Garza

----

For any additional questions or feedback, please `open an issue <https://gitlab.com/kjgarza/resplendent_quetzal/issues>`_ on Gitlab.
