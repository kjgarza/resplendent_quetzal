from .query_resolver import QueryResolver


class StrategyIntegration:
    """
    This class represents the integration of strategies for fetching data.
    """

    def __init__(self):
        self.query_resolver = QueryResolver()

    async def fetch_data(self, query_type, query):
        """
        Fetches data using the specified query type and query.

        Args:
            query_type (str): The type of query.
            query (str): The query string.

        Returns:
            tuple: A tuple containing the status code and the fetched data.

        Raises:
            ValueError: If failed to fetch data.
        """
        strategy, status_code, result = self.query_resolver.resolve(query_type, query)
        if not result:
            raise ValueError('Failed to fetch data.')
        return status_code, result
