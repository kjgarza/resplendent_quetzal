from langchain.prompts import ChatPromptTemplate

from .model_selector import ModelSelector


class CompletionService:
    def __init__(self, user):
        self.model_selector = ModelSelector()
        self.user = user

    def make(self):
        model, model_client = self.model_selector.select_model(self, self.user)
        prompt = ChatPromptTemplate.from_template('You are a helpful assistant. Help me: {topic}')
        return (prompt | model_client)
