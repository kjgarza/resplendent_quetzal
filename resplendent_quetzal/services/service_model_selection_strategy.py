from abc import ABC, abstractmethod


class ServiceModelSelectionStrategy(ABC):

    @abstractmethod
    def select_model(self, service_context, request):
        pass
