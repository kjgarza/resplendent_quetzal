from transformers import AutoModel


class ModelManager:
    def __init__(self):
        self.models = {}

    async def load_model(self, model_name):
        # This can be extended to include error handling and caching
        model = AutoModel.from_pretrained(model_name)
        self.models[model_name] = model
