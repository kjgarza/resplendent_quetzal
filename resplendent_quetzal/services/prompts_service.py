import os

import requests
import yaml

PROMPTS_REPO = 'kjgarza/prompts'
PROMPTS_FILE = 'goldie.yml'


class PromptsService:
    def __init__(self):
        self.base_url = 'https://api.github.com/repos'
        self.token = os.getenv('GITHUB_TOKEN')
        self.data = None

    def get_yaml_file(self, repo_name=PROMPTS_REPO, file_name=PROMPTS_FILE):
        headers = {
            'Authorization': f'token {self.token}',
            'Accept': 'application/vnd.github.v3.raw',
        }
        response = requests.get(
            f'{self.base_url}/{repo_name}/contents/{file_name}',
            headers=headers,
        )
        if response.status_code == 200:
            return response.text
        else:
            response.raise_for_status()

    def get_prompts(self, repo_name=PROMPTS_REPO, file_name=PROMPTS_FILE):
        yaml_file_content = self.get_yaml_file(repo_name, file_name)
        return yaml.safe_load(yaml_file_content)

    async def load_data(self):
        self.data = self.get_prompts()
