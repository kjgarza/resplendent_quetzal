import requests


class CrossrefRESTStrategy:
    """
    A class representing a strategy for fetching data from the Crossref API.

    Attributes:
        None

    Methods:
        fetch(query): Fetches data from the Crossref API based on the given query.

    """

    def fetch(self, query):
        """
        Fetches data from the Crossref API based on the given query.

        Args:
            query (str): The query to be used for fetching data from the Crossref API.

        Returns:
            tuple: A tuple containing the status code of the response and the message from the API.

        Raises:
            ValueError: If the API response is not successful or if the response is invalid.

        """
        # Your actual logic for calling Crossref API goes here
        # For demonstration, let's assume we call an endpoint and get a JSON response
        url = f'https://api.crossref.org/works/{query}'
        response = requests.get(url)
        if response.status_code != 200:
            raise ValueError('Failed to fetch data from Crossref')
        data = response.json()
        if 'message' not in data:
            raise ValueError('Invalid response from Crossref')
        return response.status_code, data['message']


class DataciteGraphQLStrategy:
    """
    A class representing a strategy for fetching data from Datacite using GraphQL.
    """

    def fetch(self, query):
        """
        Fetches data from Datacite using GraphQL.

        Args:
            query (str): The GraphQL query.

        Returns:
            tuple: A tuple containing the response status code and the data.
        """
        response = requests.post('https://api.datacite.org/graphql', json={'query': query})
        if response.status_code != 200:
            raise ValueError('Failed to fetch data from Datacite')
        data = response.json()
        if 'data' not in data:
            raise ValueError('Datacite returned invalid response')
        return response.status_code, data

    def return_grei_repositories(self):
        """
        Returns the repositories from Datacite that match specific names.

        Returns:
            tuple: A tuple containing the response status code and the data.
        """
        query = """
        {
            repositories(query: \"name:Mendeley OR name:dataverse OR name:figshare OR name:osf OR name:vivly OR
            name:zenodo OR name:dryad\") {
                totalCount
                nodes {
                    uid
                    name
                    description
                    url
                }
            }
        }
        """
        return self.fetch(query)
