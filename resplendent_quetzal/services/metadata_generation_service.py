import os

from langchain.prompts.chat import (
    ChatPromptTemplate,
    HumanMessagePromptTemplate,
    SystemMessagePromptTemplate,
)

from .model_selector import ModelSelector


class MetadataGenerationService:
    def __init__(self, user):
        self.model_selector = ModelSelector()
        self.user = user
        self.model, self.model_client = self.model_selector.select_model(
            self, self.user
        )

    def get_default_system_message(self):
        system_template = os.environ.get('GENERATION_PROMPT')

        human_template = """
        Information: {metadata}

        Generate the metadata file using the information following the  {target_schema} schema.

        """

        prompt = [
            SystemMessagePromptTemplate.from_template(system_template),
            HumanMessagePromptTemplate.from_template(human_template),
        ]

        return ChatPromptTemplate.from_messages(prompt)

    def make(self):
        model, model_client = self.model_selector.select_model(self, self.user)
        prompt = self.get_default_system_message()
        return prompt | model_client
