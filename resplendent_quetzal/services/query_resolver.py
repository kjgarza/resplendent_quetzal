from .api_strategy import CrossrefRESTStrategy, DataciteGraphQLStrategy
from .cache_resolver import cache_resolver


class QueryResolver:
    """
    A class that resolves queries based on the query type.

    Attributes:
        None

    Methods:
        resolve: Resolves the query based on the query type.

    """

    @cache_resolver(ttl=604800)
    def resolve(self, query_type, query):
        """
        Resolves the query based on the query type.

        Args:
            query_type (str): The type of the query.
            query (str): The query to be resolved.

        Returns:
            tuple: A tuple containing the strategy used, the status code, and the result.

        Raises:
            ValueError: If the query type is unsupported.

        """
        if query_type == 'journalArticle':
            strategy = CrossrefRESTStrategy()
        elif query_type in ['datasets', 'software', 'preprints', 'respositories']:
            strategy = DataciteGraphQLStrategy()
        else:
            raise ValueError(f'Unsupported query type: {query_type}')

        status_code, result = strategy.fetch(query)

        return strategy, status_code, result
