import json
import os

import yaml

import resplendent_quetzal.services.prompt_generator as prompt_generator

HOST = os.getenv('HOST')


def generate_ai_plugin_json():
    """
    Generates the AI plugin JSON data.

    Returns:
        dict: The generated AI plugin JSON data.
    """
    with open('static/ai-plugin_template.json', 'r') as jsonFile:
        data = json.load(jsonFile)
        data['api']['url'] = '{}/openapi.yaml'.format(HOST)
        data['logo_url'] = '{}/logo.png'.format(HOST)
        data['description_for_model'] = '{}'.format(prompt_generator.generate_prompt())
    return data


# Generate openapi.yaml by replacing the host
def generate_openapi_yaml():
    """
    Generate the OpenAPI YAML file by loading the template and updating the server URL.

    Returns:
        str: The generated OpenAPI YAML content.
    """
    with open('static/openapi_template.yaml', 'r') as yamlFile:
        data = yaml.load(yamlFile, Loader=yaml.FullLoader)
        data['servers'][0]['url'] = '{}'.format(HOST)

    return yaml.dump(data, default_flow_style=False)
