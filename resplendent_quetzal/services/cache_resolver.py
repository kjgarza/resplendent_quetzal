import base64
import hashlib
import json
import os

from ..redis_client import CacheClientStrategy


def cache_resolver(ttl=3600):
    """
    Decorator function that caches the result of a function using Redis.

    Args:
        ttl (int): Time-to-live in seconds for the cached result. Default is 3600 seconds (1 hour).

    Returns:
        function: Decorated function that caches its result.
    """
    def decorator(func):
        def wrapper(*args, **kwargs):
            # Check if Redis is working
            if os.getenv('ENV') == 'testing':
                return func(*args, **kwargs)
            rd_client = CacheClientStrategy().client
            if not rd_client:
                return func(*args, **kwargs)
            # Construct a unique key for this query and its arguments
            key = construct_cache_key(args)

            # Try to get the cached result
            cached_result = rd_client.get(key)
            if cached_result:
                return args[0], args[1], json.loads(decode_encoded_value(cached_result))

            # If not cached, call the resolver and cache its result
            result = func(*args, **kwargs)
            if result[0] == 200:
                rd_client.set(key, ttl, encode_result(json.dumps(result[2])))
            return result
        return wrapper
    return decorator


def construct_cache_key(args):
    sorted_args_json = json.dumps(args[2], sort_keys=True)
    raw_key = f'{args[1]}:{sorted_args_json}'
    return hashlib.sha256(raw_key.encode()).hexdigest()


def encode_result(result):
    encoded_value = base64.b32encode(result.encode()).decode()
    return encoded_value


def decode_encoded_value(encoded_value):
    decoded_value = base64.b32decode(encoded_value)
    return decoded_value
