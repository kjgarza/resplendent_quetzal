import yaml

from .prompts_service import PromptsService


def generate_prompt():
    service = PromptsService()
    yaml_file_content = service.get_yaml_file('kjgarza/prompts', 'goldie.yml')
    data = yaml.safe_load(yaml_file_content)
    system_attribute = data['system']
    prompt = system_attribute.replace('\n', ' ').replace('\t', ' ').replace('  ', ' ')
    return prompt
