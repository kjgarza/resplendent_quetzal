# from ..models.huggingface_model import HuggingFaceModel
from langchain_openai import ChatOpenAI

from .service_model_selection_strategy import ServiceModelSelectionStrategy


class ModelSelector:
    def __init__(self):
        self.strategies = {
            ('MetadataTranslationService', True): MetadataTranslationModelStrategy(),
            ('MetadataTranslationService', False): NonAuthorizedUserModelStrategy(),
            ('MetadataEnrichmentService', True): MetadataEnrichmentModelStrategy(),
            ('MetadataEnrichmentService', False): NonAuthorizedUserModelStrategy(),
            ('MetadataGenerationService', True): MetadataGenerationModelStrategy(),
            ('CompletionService', True): CompletionsModelStrategy(),
            ('CompletionService', False): NonAuthorizedUserModelStrategy(),
        }

    def select_model(self, service_context, user):
        key = (service_context.__class__.__name__, user.is_authorized)
        strategy = self.strategies.get(key)
        if not strategy:
            raise ValueError(f'No strategy found for context {key}')
        return strategy.select_model()


class CompletionsModelStrategy(ServiceModelSelectionStrategy):
    def select_model(self):
        model = 'gpt-3.5-turbo'
        model_client = ChatOpenAI(model='gpt-3.5-turbo')
        return model, model_client


class MetadataEnrichmentModelStrategy(ServiceModelSelectionStrategy):
    def select_model(self):
        # Logic to select model specifically for MetadataEnrichmentService
        model = 'gpt-3.5-turbo'
        model_client = ChatOpenAI(model='gpt-3.5-turbo')
        return model, model_client


class MetadataTranslationModelStrategy(ServiceModelSelectionStrategy):
    def select_model(self):
        # Logic to select model specifically for MetadataTranslationService
        model = 'gpt-4'
        model_client = ChatOpenAI(model='gpt-4')
        return model, model_client


class MetadataGenerationModelStrategy(ServiceModelSelectionStrategy):
    def select_model(self):
        # Logic to select model specifically for MetadataGenerationService
        model = 'gpt-3.5-turbo'
        model_client = ChatOpenAI(model='gpt-3.5-turbo')
        return model, model_client


class NonAuthorizedUserModelStrategy(ServiceModelSelectionStrategy):
    def select_model(self):
        # Logic to select model specifically for MetadataGenerationService
        model = 'gpt-3.5-turbo'
        model_client = ChatOpenAI(model='gpt-3.5-turbo')
        return model, model_client
