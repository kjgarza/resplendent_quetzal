from abc import ABC, abstractmethod


class ModelSelectionStrategy(ABC):

    @abstractmethod
    def select_model(self, request):
        pass
