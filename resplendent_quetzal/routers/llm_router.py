import os

from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from langserve import add_routes
from pydantic import BaseModel

from resplendent_quetzal.services.completion_service import CompletionService
from resplendent_quetzal.services.metadata_enrichment_service import (
    MetadataEnrichmentService,
)
from resplendent_quetzal.services.metadata_generation_service import (
    MetadataGenerationService,
)
from resplendent_quetzal.services.metadata_translation_service import (
    MetadataTranslationService,
)

from ..services.prompts_service import PromptsService


class User(BaseModel):
    is_authorized: bool


security = HTTPBasic()

# Set the environment variables for the prompts
# TODO: Move this to a lifespan when https://github.com/tiangolo/fastapi/discussions/9664 gets fixed
prompts = PromptsService().get_prompts('kjgarza/prompts', 'parrot_gpt.yml')
os.environ['ENRICHMENT_PROMPT'] = prompts['enrich']['system']
os.environ['TRANSLATION_PROMPT'] = prompts['traslate_improved']['system']
os.environ['GENERATION_PROMPT'] = prompts['generate']['system']


async def verify_token(credentials: HTTPBasicCredentials = Depends(security)) -> None:
    username = os.getenv('USERNAME')
    password = os.getenv('PASSWORD')

    if credentials.username == username and credentials.password == password:
        user = User(is_authorized=True)
        user.is_authorized = True
        return user
    else:
        raise HTTPException(status_code=401, detail='Unauthorized')

llm_router = APIRouter(dependencies=[Depends(verify_token)])

add_routes(
    llm_router,
    CompletionService(User(is_authorized=True)).make(),
    path='/complete'
    )

add_routes(
    llm_router,
    MetadataEnrichmentService(User(is_authorized=True)).make(),
    path='/enrich'
    )

add_routes(
    llm_router,
    MetadataTranslationService(User(is_authorized=True)).make(),
    path='/translate'
    )

add_routes(
    llm_router,
    MetadataGenerationService(User(is_authorized=True)).make(),
    path='/generate'
    )
