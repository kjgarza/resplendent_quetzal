from ariadne import QueryType, make_executable_schema
from ariadne.asgi import GraphQL
from ariadne.load_schema import load_schema_from_path
from fastapi import APIRouter, Request

from resplendent_quetzal.services.strategy_integration import StrategyIntegration

router = APIRouter()

query = QueryType()


@query.field('repositories')
async def repositories(_, info, **kwargs):
    query = await info.context.get('request').json()
    status_code, result = await StrategyIntegration().fetch_data(
        'datasets',
        query['query']
    )
    return result['data']['repositories']


@query.field('repository')
async def repository(_, info, **kwargs):
    query = await info.context.get('request').json()
    status_code, result = await StrategyIntegration().fetch_data('datasets', query['query'])
    return result['data']['repository']

type_defs = load_schema_from_path(
    'static/schema.graphql'
)

schema = make_executable_schema(type_defs, query)


# Custom context setup method
def get_context_value(request_or_ws: Request, _data) -> dict:
    return {
        'request': request_or_ws
    }


# Create GraphQL App instance
graphql_app = GraphQL(
    schema,
    debug=True,
    context_value=get_context_value
)


# Handle GET requests to serve GraphQL explorer
# Handle OPTIONS requests for CORS
@router.get('/graphql/')
@router.options('/graphql/')
async def handle_graphql_explorer(request: Request):
    return await graphql_app.handle_request(request)


# Handle POST requests to execute GraphQL queries
@router.post('/graphql/')
async def handle_graphql_query(request: Request):
    return await graphql_app.handle_request(request)
