import os
from typing import List

from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from pydantic import BaseModel

from resplendent_quetzal.services.completion_service import CompletionService
from resplendent_quetzal.services.metadata_enrichment_service import (
    MetadataEnrichmentService,
)
from resplendent_quetzal.services.metadata_generation_service import (
    MetadataGenerationService,
)
from resplendent_quetzal.services.metadata_translation_service import (
    MetadataTranslationService,
)

router = APIRouter()


# Request and Response models
class Message(BaseModel):
    role: str
    content: str


class CompletionRequest(BaseModel):
    model: str
    messages: List[Message]


class TranslationRequest(BaseModel):
    model: str
    metadata: str
    source_format: str
    target_format: str


class EnrichmentRequest(BaseModel):
    model: str
    metadata: str
    extra: str


class GenerationRequest(BaseModel):
    context: str
    target_schema: str


class CompletionResponse(BaseModel):
    id: str
    choices: List[dict]


class User(BaseModel):
    is_authorized: bool


security = HTTPBasic()


def get_current_user(credentials: HTTPBasicCredentials = Depends(security)):
    username = os.getenv('USERNAME')
    password = os.getenv('PASSWORD')

    if credentials.username == username and credentials.password == password:
        user = User(is_authorized=True)
        user.is_authorized = True
        return user
    else:
        raise HTTPException(status_code=401, detail='Unauthorized')


# API Endpoints
@router.post('/completions', response_model=CompletionResponse)
async def completions_endpoint(
    request: CompletionRequest,
    user: User = Depends(get_current_user)
):
    try:
        # Instantiate the MetadataTranslationService with the current user
        completion_service = CompletionService(user)

        # Call the service to perform the completion
        result = await completion_service.generate_completion(request)

        # Return the result
        return CompletionResponse(id='dsadsac', choices=[dict(result)])
    except Exception as e:
        # Handle exceptions and errors appropriately
        raise HTTPException(status_code=500, detail=str(e))


@router.post('/metadata/translate')
async def metadata_translate_endpoint(
    request: TranslationRequest,
    user: User = Depends(get_current_user)
):
    try:
        # Instantiate the MetadataTranslationService with the current user
        translation_service = MetadataTranslationService(user)

        # Call the service to perform the translation
        result = await translation_service.translate(request)

        # Return the result
        return result
    except Exception as e:
        # Handle exceptions and errors appropriately
        raise HTTPException(status_code=500, detail=str(e))


@router.post('/metadata/enrich')
async def metadata_enrich_endpoint(
        request: EnrichmentRequest,
        user: User = Depends(get_current_user)
):
    try:
        # Instantiate the MetadataTranslationService with the current user
        enrichment_service = MetadataEnrichmentService(user)

        # Call the service to perform the enrichment
        result = await enrichment_service.enrich(request)

        # Return the result
        return result
    except Exception as e:
        # Handle exceptions and errors appropriately
        raise HTTPException(status_code=500, detail=str(e))


@router.post('/metadata/generate')
async def metadata_generate_endpoint(
    request: GenerationRequest,
    user: User = Depends(get_current_user)
):
    try:
        # Instantiate the MetadataTranslationService with the current user
        generation_service = MetadataGenerationService(user)

        # Call the service to perform the generation
        result = await generation_service.generate(request)

        # Return the result
        return result
    except Exception as e:
        # Handle exceptions and errors appropriately
        raise HTTPException(status_code=500, detail=str(e))
