from fastapi import APIRouter
from fastapi.encoders import jsonable_encoder
from fastapi.responses import FileResponse, HTMLResponse, JSONResponse, Response
from fastapi.templating import Jinja2Templates
from starlette.requests import Request

import resplendent_quetzal.services.static_files_generator as static_files

router = APIRouter()
templates = Jinja2Templates(directory='resplendent_quetzal/templates')


@router.get('/.well-known/ai-plugin.json', include_in_schema=False)
async def get_plugin_file():
    data = static_files.generate_ai_plugin_json()
    return JSONResponse(
        content=jsonable_encoder(data),
        media_type='application/json'
    )


@router.get('/openapi.yaml', include_in_schema=False)
async def get_openapi_file():
    data = static_files.generate_openapi_yaml()
    return Response(content=data, media_type='text/yaml')


@router.get('/logo.png', include_in_schema=False)
async def get_logo_file():
    return FileResponse('static/logo.png', media_type='image/png')


@router.get('/', response_class=HTMLResponse)
async def read_root(request: Request):
    return templates.TemplateResponse('ft.html', {'request': request})


@router.get('/goldie', response_class=HTMLResponse, include_in_schema=False)
async def read_landing_page(request: Request):
    return templates.TemplateResponse('retriever.html', {'request': request})


@router.get('/inmediasres.svg', include_in_schema=False)
async def read_logo(request: Request):
    return FileResponse('static/inmediasres.svg', media_type='image/svg+xml')
