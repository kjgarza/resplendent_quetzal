import os
from typing import Optional

import redis
import vercel_kv


class CacheClientStrategy:
    """
    Represents a cache client strategy that determines the type of client to use based on environment variables.
    """

    def __init__(self):
        self.client = self.get_client()

    def get_client(self):
        """
        Returns the appropriate cache client based on the environment variables.

        If the 'VERCEL_KV_URL' environment variable is set, returns a VercelKvClient instance.
        Otherwise, returns a RedisClient instance.
        """
        if os.getenv('KVR_URL'):
            os.environ['VERCEL_KV_URL'] = os.getenv('KVR_URL')
            os.environ['VERCEL_KV_REST_API_URL'] = os.getenv('KVR_REST_API_URL')
            os.environ['VERCEL_KV_REST_API_TOKEN'] = os.getenv('KVR_REST_API_TOKEN')
            os.environ['VERCEL_KV_REST_API_READ_ONLY_TOKEN'] = os.getenv('KVR_REST_API_READ_ONLY_TOKEN')

        if os.getenv('VERCEL_KV_URL'):
            return VercelKvClient()
        else:
            return RedisClient()


class VercelKvClient:
    """
    A client for interacting with Vercel KV.
    """

    def __init__(self):
        self.client = self.get_client()

    def get_client(self):
        """
        Get the Vercel KV client.
        """
        kv_client = vercel_kv.KV()
        return kv_client

    def get(self, key):
        """
        Get the value associated with the given key from Vercel KV.

        Args:
            key (str): The key to retrieve the value for.

        Returns:
            The value associated with the key.
        """
        return self.client.get(key)

    def set(self, key, ttl, value):
        """
        Set the value for the given key in Vercel KV.

        Args:
            key (str): The key to set the value for.
            ttl (int): The time-to-live (TTL) for the value in seconds.
            value: The value to set.

        Returns:
            bool: True if the value was set successfully, False otherwise.
        """
        r = Opts()
        r.ex = ttl
        return self.client.set(key, value, r)


class RedisClient:
    """
    A class representing a Redis client.

    This class provides methods to interact with a Redis server.

    Attributes:
        client (redis.Redis): The Redis client instance.

    Methods:
        get_client(): Returns a Redis client instance.
        get(key): Retrieves the value associated with the given key.
        set(key, ttl, value): Sets the value associated with the given key.

    """

    def __init__(self):
        self.client = self.get_client()

    def get_client(self):
        """
        Returns a Redis client instance.

        Returns:
            redis.Redis: A Redis client instance.

        """
        REDIS_HOST = os.getenv('REDIS_HOST')
        REDIS_PORT = os.getenv('REDIS_PORT')
        REDIS_DB = os.getenv('REDIS_DB')
        redis_client = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
        return redis_client

    def get(self, key):
        """
        Retrieves the value associated with the given key.

        Args:
            key (str): The key to retrieve the value for.

        Returns:
            bytes: The value associated with the given key.

        """
        return self.client.get(key)

    def set(self, key, ttl, value):
        """
        Sets the value associated with the given key.

        Args:
            key (str): The key to set the value for.
            ttl (int): The time-to-live (TTL) for the key-value pair in seconds.
            value (bytes): The value to set for the key.

        Returns:
            bool: True if the value was set successfully, False otherwise.

        """
        return self.client.set(key, value, ttl)


class Opts():
    ex: Optional[int]
    px: Optional[int]
    exat: None
    pxat: None
    keepTtl: None
