from ariadne.explorer import ExplorerPlayground
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles

from . import config
from .resources import lifespan
from .routers import graphql, static
from .routers.llm_router import llm_router

PLAYGROUND_HTML = ExplorerPlayground(title='Cool API').html(None)

app = FastAPI(
    title='In Medias Res API',
    description='API for FAIR Scholarly metadata discovery and generation',
    debug=config.DEBUG,
    lifespan=lifespan,
)

app.mount('/static', StaticFiles(directory='static'), name='static')

# List of allowed origins. You can use ["*"] to allow all origins
origins = [
    'https://chat.openai.com',
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=['*'],  # Allows all methods, you can specify ["GET", "POST"] to restrict it
    allow_headers=['*'],  # Allows all headers, you can specify what headers are allowed
)

routers = (
    graphql.router,
    static.router,
)

for router in routers:
    app.include_router(router)
    app.include_router(llm_router)
