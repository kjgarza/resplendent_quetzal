from collections.abc import AsyncIterator
from contextlib import asynccontextmanager
import os
from fastapi import FastAPI
from loguru import logger
from .services.prompts_service import PromptsService


from . import config


@asynccontextmanager
async def lifespan(app: FastAPI) -> AsyncIterator:
    startup()
    try:
        yield
    finally:
        await shutdown()

def startup() -> None:
    show_config()
    logger.info('started...')
    # model_manager = ModelManager()
    # await model_manager.load_model("bert-base-uncased")
    # # Add more models as needed
    # logger.info('models loaded...')


async def shutdown() -> None:
    # insert here calls to disconnect from database and other services
    logger.info('...shutdown')


def show_config() -> None:
    config_vars = {key: getattr(config, key) for key in sorted(dir(config)) if key.isupper()}
    logger.debug(config_vars)
