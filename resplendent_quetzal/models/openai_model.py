import os

from openai import OpenAI


class OpenaiModel:
    def __init__(self):
        api_key = os.environ.get('OPENAI_API_KEY')
        self.client = OpenAI(api_key=api_key)

    async def fetch_completion(self, model, messages, task=None):
        try:
            completion = self.client.chat.completions.create(
                model=model,
                messages=messages,
                temperature=0.2,
                top_p=1.0,
                frequency_penalty=0.0,
                presence_penalty=0.0
            )

            return completion.choices[0].message
        except Exception as e:
            # Handle exceptions and errors appropriately
            raise e
