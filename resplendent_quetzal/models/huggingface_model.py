import os

from transformers import pipeline


class HuggingFaceModel:
    def __init__(self):
        self.api_key = os.environ.get('HUGGINGFACE_API_KEY')
        self.client = pipeline

    async def fetch_completion(self, model, messages):
        print('messages', messages)
        task, prompt = self.format_prompt(messages)
        try:
            completion = self.client(
                model=model,
                task=task,
                token=self.api_key,
            )
            result = {'content': completion(prompt)}
            print('result', result)
            return result
        except Exception as e:
            # Handle exceptions and errors appropriately
            raise e

    def format_prompt(self, messages):
        prompt = ''
        task = ''
        for message in messages:
            match message['role']:
                case 'system':
                    task = message['content']
                case 'user':
                    prompt = message['content']
        return task, prompt
